package polymorphism;

public class BookStore {
    public static void main (String[] args) {
        Book[] b = new Book[5];
        b[0] = new Book("Family Guy", "Seth McFarlane");
        b[1] = new ElectronicBook("Giancarlo Passariello", "Cool Book", 2);
        b[2] = new Book("Don Quixote", "Miguel de Cervantes");
        b[3] = new ElectronicBook("The Hobbit", "J.R.R. Tolkien", 4);
        b[4] = new ElectronicBook("The Da Vinci Code", "Dan Brown", 10);
    
        for (int i = 0; i < b.length; i++) {
            System.out.println(b[i]);
        }
    
    }
}
