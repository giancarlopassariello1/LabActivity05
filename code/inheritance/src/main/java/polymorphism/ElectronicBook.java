package polymorphism;

public class ElectronicBook extends Book {
    private double numberBytes;

    public ElectronicBook(String newTitle, String newAuthor, double newNumberBytes) {
        super(newTitle, newAuthor);
        this.numberBytes = newNumberBytes;
    }

    public double getNumberBytes() {
        return this.numberBytes;
    }

    @Override
    public String toString() {
        String s = super.toString();
        return s;
    }
}
