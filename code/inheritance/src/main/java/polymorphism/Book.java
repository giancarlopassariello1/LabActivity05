package polymorphism;

public class Book {
    protected String title;
    private String author;

    public Book(String newTitle, String newAuthor) {
        this.title = newTitle;
        this.author = newAuthor;
    }

    public String getTitle() {
        return this.title;
    }

    public String getAuthor() {
        return this.author;
    }

    @Override
    public String toString() {
        return "Title: "+this.title+" / Author: "+this.author;
    }
}
